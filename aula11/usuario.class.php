<?php
class Usuario {


    private $id;
    private $nome;
    private $email;
    private $senha;
    public function __construct(){
        echo "<br>Aqui será feito a conexâo com o SGDB <br>";
    }

    public function setId (int $id) {
        $this ->id = $id;
    }

    public function setNome (string $nome) {
        $this ->nome = $nome;
    }

    public function setEmail (string $email){
        $this ->email = $email;
    }

    public function setSenha (string $senha){
        $this ->senha = $senha;
    }

    public function getId (int $id) {
       return $this ->id = $id;
    }

    public function getNome (string $nome) {
        return $this ->nome = $nome;
    }
    
    public function getEmail (string $email) {
        return $this ->email = $email;
    }

    public function getSenha (string $senha) {
        return $this ->senha = $senha;
    }
    public function __destruct(){
        echo "<br> Fechamento a conexão com o SGBD";
    }


}