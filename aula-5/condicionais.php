<?php
echo '<pre>';

$alunos = [];
$nomes = ['Matheus Oliveira', 'Mikael Assis', 'Leandro Ramos', 'Gabrielly Vieira', 'Mayara Manso'];
$bitbucket = ['https://bitbucket.org/kirbyns', 'https://bitbucket.org/kirbyns', 'https://bitbucket.org/kirbyns', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso'];

for($l=0; $l<5; $l++){
    $alunos[$l]['nome'] = $nomes[$l];
    $alunos[$l]['bitbucket'] = $bitbucket[$l];
}

// var_dump($alunos);

##### EXPLICAÇÂO PROFESSOR #####
echo '<table border="1">
        <thead>
            <th>
                Nome
            </th>
            <th>
                Bitbucket
            </th>
        </thead>';
    $cor = 'white';
for($i=0; $i < count($alunos); $i++){
    echo "  <tr>
                <td>
                {$alunos[$i]['nome']}
                </td>
                <td>
                {$alunos[$i]['bitbucket']}
                </td>
            </tr>";
};

echo '</table>';

//AGORA COM FOREACH :-)
/*
foreach (VARIAVEL as INDICE => VALOR) {
    # code...
}
*/
echo '<p>Resolução com Foreach e operador ternario</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';


foreach ($alunos as $ind => $linha) {
    $cor = $cor == 'gray' ? 'white': 'gray';
    echo "  <tr bgcolor ='$cor'>
                <td>
                    {$linha['nome']}
                </td>
                <td>
                    {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table>';


//Tabela linda agora com if 

echo '<p>Resolução com IF</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';

$cor = 'white';
foreach ($alunos as $ind => $linha) {
    if($cor=='gray'){
        $cor ='white'; }
    else {
        $cor = 'gray';
    }    
        
    echo "  <tr bgcolor ='$cor'>
                <td>
                    {$linha['nome']}
                </td>
                <td>
                    {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table>';

//Tabela linda  outro operador 

echo '<p>Resolução com IF</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';

$cor = 'white';
foreach ($alunos as $ind => $linha) {
    if($cor=='gray'){
        $cor ='white'; }
    else {
        $cor = 'gray';
    }    
        
    echo "  <tr bgcolor ='$cor'>
                <td>
                    {$linha['nome']}
                </td>
                <td>
                    {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table><br><br>';

// Operadores === e !==

$nome = 'Luiz Fernando Albertin Bono Milan';

$posicao = strpos( $nome, 'Luiz');

if( $posicao !== false){
    echo "encontramos na posição $posicao!!";

}

